import redpitaya_scpi as scpi
import numpy as np
import time


def start(IP):
    rp = scpi.scpi(IP)
    return rp


def setup(rp, output=1, waveform='sine', frequency=1000, amplitude=0, burst_mode=False, bursts=1,
          b_reps=1, b_time=None, data=None):

    waveforms = {'sine', 'square', 'triangle', 'sawu', 'sawd', 'pwm', 'arbitrary'}
    # Check validity of output
    if output == 1:
        source = 'SOUR1'
    elif output == 2:
        source = 'SOUR2'
    else:
        raise Exception(f'Output = {output} is not a valid input, only 1 or 2.')

    # Check validity of waveform
    if waveform in waveforms:
        rp.tx_txt(source + ':FUNC ' + str(waveform).upper())
        if waveform == 'arbitrary':
            rp.tx_txt(source + ':TRAC:DATA:DATA ' + ','.join(map(str, data)))
    else:
        raise Exception(f'Invalid waveform. Possible waveforms: {waveforms}.')

    # set frequency
    if 0 <= frequency <= 62.5e6:
        rp.tx_txt(source + ':FREQ:FIX ' + str(frequency))
    else:
        raise Exception(f'Frequency should be between 0 Hz and 62.5e6 Hz.')

    # set amplitude
    if -1 <= amplitude <= 1:
        rp.tx_txt(source + ':VOLT ' + str(amplitude))
    else:
        raise Exception(f'Amplitude should be between -1 V and 1 V.')

    # Burst mode
    if burst_mode is True:
        rp.tx_txt(source + ':BURS:STAT ON')
        # nr of bursts
        rp.tx_txt(source + ':BURS:NCYC ' + str(bursts))
        rp.tx_txt(source + ':BURS:NOR ' + str(b_reps))
        if b_time is not None and (500e6 > b_time > 1):
            rp.tx_txt(source + ':BURS:INT:PER ' + str(int(b_time)))  # us
    elif burst_mode is False:
        pass
    else:
        raise Exception(f'Burst mode is True or False.')


def on(rp, output):
    rp.tx_txt('OUTPUT' + str(output) + ':STATE ON')


def off(rp):
    rp.tx_txt('OUTPUT1:STATE OFF')
    rp.tx_txt('OUTPUT1:STATE OFF')


def reset(rp):
    rp.tx_txt('GEN:RST')


def trigger(rp, output):
    if output == 'both':
        rp.tx_txt('SOUR:TRIG:IMM')
    else:
        rp.tx_txt('SOUR' + str(output) + ':TRIG:IMM')


def setup_scope(rp, gain1, gain2, decimation=1, trigger_level=0, trigger_delay=0, trigger_source='awg_pe'):
    rp.tx_txt('ACQ:RST')  # reset scope
    _decimations = {2 ** n: 2 ** n for n in range(0, 17)}
    _max_samples = (2 ** 14)

    # <decimation> = {1,8,64,1024,8192,65536} 2**0 --> 2**16
    if decimation in _decimations.values():
        rp.tx_txt('ACQ:DEC ' + str(decimation))
    else:
        raise Exception(f'Possible decimations: {_decimations.values()}')

    if -1e3 < trigger_level < 1e3:
        rp.tx_txt('ACQ:TRIG:lEV ' + str(trigger_level))  # in mV
    else:
        raise Exception(f'Trigger level should be between -1e3 mV and 1e3 mV.')

    # 0 samples delay set trigger to center of the buffer
    # Signal on your graph will have trigger in the center (symmetrical)
    # Samples from left to the center are samples before trigger
    # Samples from center to the right are samples after trigger

    if -_max_samples / 2 <= trigger_delay <= _max_samples / 2:
        rp.tx_txt('ACQ:TRIG:DLY ' + str(trigger_delay))  # in samples
    else:
        raise Exception(f'Trigger delay should be between {-_max_samples} and {_max_samples}.')

    rp.tx_txt('ACQ:START')
    time.sleep(1)

    if (gain1.upper() and gain2.upper()) in {'LV', 'HV'}:
        rp.tx_txt('ACQ:SOUR1:GAIN ' + gain1.upper())
        rp.tx_txt('ACQ:SOUR2:GAIN ' + gain2.upper())
    else:
        raise Exception(f'Gain can either be "LV" or "HV".')

    # trigger source setting MUST be after 'ACQ:START'
    # AWG = arbitrary signal generator, PE = postitive edge, NE = negative edge
    _trigger_sources = {'DISABLED', 'NOW', 'CH1_PE', 'CH1_NE', 'CH2_PE',
                        'CH2_NE', 'EXT_PE', 'EXT_NE', 'AWG_PE', 'AWG_NE'}

    if trigger_source.upper() in _trigger_sources:
        rp.tx_txt('ACQ:TRIG ' + str(trigger_source).upper())
    else:
        raise Exception(f'Possible oscillscope trigger sources are: {_trigger_sources}.')


def acquire(rp, channel=1, awg_trigger_source='imm'):
    if str(channel).upper() == 'BOTH':

        rp.tx_txt('SOUR:TRIG:IMM')
        # print('both')

        # Should calculate actual needed time...not 1 second
        while 1:
            rp.tx_txt('ACQ:TRIG:STAT?')
            if rp.rx_txt() == 'TD':  # Done
                break

        rp.tx_txt('ACQ:SOUR1:DATA?')
        buffer_str1 = rp.rx_txt()
        buffer_str1 = buffer_str1.strip('{}\n\r').split(',')
        array1 = np.fromiter(buffer_str1, dtype='float32')
        rp.tx_txt('ACQ:SOUR2:DATA?')
        buffer_str2 = rp.rx_txt()
        buffer_str2 = buffer_str2.strip('{}\n\r').split(',')
        array2 = np.fromiter(buffer_str2, dtype='float32')

        rp.tx_txt('ACQ:STOP')
        rp.tx_txt('OUTPUT1:STATE OFF')
        rp.tx_txt('OUTPUT2:STATE OFF')

        return array1, array2

    elif channel in {1, 2}:
        # <trigger> = {EXT_PE, EXT_NE, INT, GATED}
        _awg_trigger_sources = {'IMM', 'EXT_PE', 'EXT_NE', 'INT', 'GATED'}

        if awg_trigger_source.upper() in _awg_trigger_sources:
            if awg_trigger_source.upper() == 'IMM':
                rp.tx_txt('SOUR' + str(channel) + ':TRIG:' + str(awg_trigger_source).upper())
            else:
                rp.tx_txt('SOUR' + str(channel) + ':TRIG:SOUR ' + str(awg_trigger_source).upper())
        else:
            raise Exception(f'Possible generator trigger sources are: {_awg_trigger_sources}.')
        # print('single')
        # Should calculate actual needed time...not 1 second
        while 1:
            rp.tx_txt('ACQ:TRIG:STAT?')
            if rp.rx_txt() == 'TD':  # Done
                break

        rp.tx_txt('ACQ:SOUR' + str(channel) + ':DATA?')
        buffer_str = rp.rx_txt()
        buffer_str = buffer_str.strip('{}\n\r').split(',')
        array = np.fromiter(buffer_str, dtype='float32')

        rp.tx_txt('ACQ:STOP')
        rp.tx_txt('OUTPUT1:STATE OFF')
        rp.tx_txt('OUTPUT2:STATE OFF')
        return array

    else:
        raise Exception(f'Possible channels are: 1, 2, or "both".')
