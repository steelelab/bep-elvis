import numpy as np
from pandas import read_csv
from scipy.ndimage import shift


def csv_to_array(filename, channels=2):
    df = read_csv(filename)
    array = df.to_numpy()
    if channels == 1:
        ch1 = np.fromiter(array[1:, 1], float)
        increment = array[0, 3]
        t = np.fromiter(array[1:, 0], float) * increment
        return ch1, t
    elif channels == 2:
        ch1 = np.fromiter(array[1:, 1], float)
        ch2 = np.fromiter(array[1:, 2], float)
        increment = array[0, 4]
        t = np.fromiter(array[1:, 0], float) * increment
        return ch1, ch2, t


def sloping_pulse(start, slope_length, pulse_width, size=2 ** 14):
    #      Entries now in ns!!
    """By default the there are three samples with value zero at the start of the pulse.
    Otherwise the DC signal starts going mayhem after. This means the earliest possible
    pulse always has a start time of 24 ns"""

    start = round(start / 8) + 2  # one sample from linspace
    slope_length = 2 + round(slope_length / 8)  # linspace of length 2 is just [0, 1] --> no slope
    pulse_width = round(pulse_width / 8) - 2

    amplitude = 1
    if size is None:
        size = start + 2 * slope_length + pulse_width + 3

    # total_pulse_length = start + 2 * slope_length + pulse_width + 3
    pulse = np.zeros(size)
    slope = np.linspace(0, amplitude, slope_length)
    pulse[start: start + slope_length] = slope
    pulse[start + slope_length + pulse_width: pulse_width + start + 2 * slope_length] = np.flip(slope, axis=0)

    pulse[start + slope_length: start + slope_length + pulse_width] = amplitude

    return pulse


def shift_ns(array, ns):
    "Rounded to a whole number of samples"

    samples = ns / 8

    return shift(array, round(samples))


def convert(mV):
    c1 = 0.15152507598316284
    c2 = 5.415185896337901
    dBm = (np.log(mV)-c2)/c1
    return dBm


def multi_pulse(tr, t1, nr_of_pulses, s_length=40):
    """Watch out as t1 approaches 0 or tr, then slope lengths start to interfere"""
    # ns --> 5 samples
    width = t1 - 2 * s_length

    pulse = sloping_pulse(start=0, slope_length=s_length, pulse_width=width)
    full_pulse = np.copy(pulse)

    for i in range(nr_of_pulses):
        full_pulse += shift_ns(pulse, tr * (i + 1))

    return full_pulse
