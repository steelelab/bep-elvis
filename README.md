<!-- #region -->
# A respository for Elvis' BEP project

## Idea

Implement pulsed measurements using a Red Pitaya board using a standard Vector Network Analyzer.


## Links

Manufacturer:

https://www.redpitaya.com/

https://redpitaya.readthedocs.io/en/latest/

PyRPL: 

https://pyrpl.readthedocs.io/en/latest/

Gary's code trying to use the RP for an RP:

https://gitlab.tudelft.nl/python-for-applied-physics/rp-squid-python-code


## Background reading

A Quantum Engineer's Guide to Superconducting Qubits
Philip Krantz, Morten Kjaergaard, Fei Yan, Terry P. Orlando, Simon Gustavsson, William D. Oliver
https://arxiv.org/abs/1904.06560

A great one for qubits start to finish, including the gory details of how to measure them. A good first objective in reading this paper:

* How does a T1 measurement work? 

<!-- #endregion -->
